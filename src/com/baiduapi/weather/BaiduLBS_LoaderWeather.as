package com.baiduapi.weather
{
	
	import com.baiduapi.Config;
	import com.baiduapi.events.BaiduLBS_LocationEvent;
	import com.baiduapi.events.BaiduLBS_WeatherEvent;
	import com.baiduapi.location$ip.BaiduLBS_LoaderLocation;
	import com.baiduapi.namespaces.Baidu_LBS;
	import com.baiduapi.weather.data.BaiDu_weather;
	
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IOErrorEvent;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	use namespace Baidu_LBS
	[Event(name="baidulbs_weather_event", type="com.baiduapi.events.BaiduLBS_WeatherEvent")]
	/**
	 * 百度LBS 天气预报
	 *@author 偷心枫贼
	 *        下午5:49:41
	 *
	 */

	public class BaiduLBS_LoaderWeather extends EventDispatcher
	{
		public static const VERSION:String="v1.0"
			
			
		private static const URL:String="http://api.map.baidu.com/telematics/v3/weather"
			
			
		public function BaiduLBS_LoaderWeather()
		{
			super()
		}
		
		
		/**
		 *  
		 * @param location 位置数组  可以为城市名或者坐标  ["广州","深圳"]  或者  ["广州","29,30.21"] 或者全坐标 ["29,30.21","90,100.22"]
		 *                   如果为null,将调用自带位置api获取(自带api可能不会准确)
		 * @param ak  用户密钥,不填时将默认为Config静态属性ak,请先配置 
		 * 
		 */		
		public function load(location:Array=null,ak:String=null):void{
		
			if(location){
				if(location.length>0){
					var loader:URLLoader=new URLLoader()
					
					loader.addEventListener(Event.COMPLETE,onComplete)
					loader.addEventListener(IOErrorEvent.IO_ERROR,onComplete)
					
					
					var req:URLRequest=new URLRequest();
					req.url=URL;
					var data:URLVariables=new URLVariables()
					data.ak=ak?ak:Config.ak
					data.location=location.join("|")
					data.output="json"
					data.coor="bd09ll"
					
					req.data=data
					req.method=URLRequestMethod.GET
										
					loader.load(req)
				}else{
					dispatchEvent(new BaiduLBS_WeatherEvent(BaiduLBS_WeatherEvent.COMPLETE,false,false,new BaiDu_weather()))
				}
				
			}else{	
				
				var lco:BaiduLBS_LoaderLocation=new BaiduLBS_LoaderLocation()
				lco.addEventListener(BaiduLBS_LocationEvent.COMPLETE,onCompleteReturnLoction)
				lco.load(null,ak)
				function onCompleteReturnLoction(e:BaiduLBS_LocationEvent):void{
					lco.removeEventListener(BaiduLBS_LocationEvent.COMPLETE,onCompleteReturnLoction)
					try
					{
						trace(e.data.content.point.toLocationString())
						load([e.data.content.point.toLocationString()],ak)
					} 
					catch(error:Error) 
					{
						
					}
										
				}	
			}
			
						
		}
		
		
		
		private function onComplete(e:Event):void{
			var loader:URLLoader=e.target as URLLoader
			loader.removeEventListener(Event.COMPLETE,onComplete)
			loader.removeEventListener(IOErrorEvent.IO_ERROR,onComplete)		
			if(e.type==Event.COMPLETE){
				var json:String=String(loader.data)
				var d:BaiDu_weather=new BaiDu_weather(JSON.parse(json))
				dispatchEvent(new BaiduLBS_WeatherEvent(BaiduLBS_WeatherEvent.COMPLETE,false,false,d))
			}else{
				dispatchEvent(new BaiduLBS_WeatherEvent(BaiduLBS_WeatherEvent.COMPLETE,false,false,new BaiDu_weather()))
			}		
			loader=null
		}
		
		
		
	}
}