package com.baiduapi.weather.data
{
	/**
	 * 城市天气状况数据 
	 * @author 偷心枫贼
	 * 
	 */	
	internal class _city
	{
		private var _index_dress:__index=null
		private var _index_washCar:__index=null
		private var _index_movement:__index=null		
		private var _index_catchCold:__index=null
		private var _index_uv:__index=null
		private var _weather_today:__weather_data=null
		private var _weather_tomorrow:__weather_data=null
		private var _weather_afterTomorrow:__weather_data=null
		private var _weather_theDay:__weather_data=null
		private var _currentCity:String=null
		
		
		
		private var _pm25:__pm25=null
		
		
		
		
		
		
		/**
		 * 今天的天气 
		 * @return 
		 * 
		 */	
		final public function get weather_today():__weather_data{return _weather_today||new __weather_data()};
		/**
		 * 明天的天气 
		 * @return 
		 * 
		 */	
		final public function get weather_tomorrow():__weather_data{return _weather_tomorrow||new __weather_data()}
		/**
		 * 后天的天气 
		 * @return 
		 * 
		 */
		final public function get weather_afterTomorrow():__weather_data{return _weather_afterTomorrow||new __weather_data()}
		
		/**
		 *  大后天的天气 
		 * @return 
		 * 
		 */	
		final public function get weather_theDay():__weather_data{return _weather_theDay||new __weather_data()}
		
		
		
		/**
		 * 城市名称 
		 */	
		final public function  get  currentCity() :String{return _currentCity};
		/**
		 * PM25 
		 */	
		final public function get  pm25():__pm25{return _pm25}
		
		/**
		 * 指数集合,分为:穿衣、洗车、感冒、运动、紫外线这几个指数
		 */	
		public const indexs:Vector.<__index>=new Vector.<__index>()
		/**
		 * 天气状况集合,白天为今天,明天，后天,   晚上为今天，明天，后天，大后天 
		 */		
		public const weather_datas:Vector.<__weather_data>=new Vector.<__weather_data>()
		
		
		/**
		 * 洗车指数 
		 */	
		final public function get index_washCar():__index{return _index_washCar||new __index()}
		
		/**
		 * 穿衣指数 
		 */		
		final public function get index_dress():__index{return _index_dress||new __index()}
		/**
		 * 感冒指数 
		 */	
		final public function get index_catchCold():__index {return _index_catchCold||new __index()}
		/**
		 *运动指数 
		 */
		
		final public function get index_movement():__index{return _index_movement||new __index()}
		/**
		 * 紫外线指数 
		 */		
		
		final public function get index_uv():__index{return _index_uv||new __index()}
		
		public function _city(obj=null)
		{
			if(obj!=null){
				
				
				_currentCity=obj.currentCity
				
				_pm25=new __pm25(obj.pm25)
				var arr:Array=obj.index//获取指数数组
				
				var i:int;
			
				//赋值指数
				for (i=0;i<arr.length;i++){
					var mindex:__index=new __index(arr[i])
					if(mindex.title=="穿衣"){
						_index_dress=mindex
					}else if(mindex.title=="洗车"){
						_index_washCar=mindex
					}else if(mindex.title=="感冒"){
						_index_catchCold=mindex
					}else if(mindex.title=="运动"){
						_index_movement=mindex
					}else if(mindex.title=="紫外线强度"){
						_index_uv=mindex
					}
					
					indexs.push(mindex);
				}
				
				arr=obj.weather_data
				
				
				for(i=0;i<arr.length;i++){
					var mweather_data:__weather_data=new __weather_data(arr[i])		
					if(i==0){
						_weather_today=mweather_data
					}else if(i==1){
						_weather_tomorrow=mweather_data
					}else if(i==2){
						_weather_afterTomorrow=mweather_data
					}else if(i==3){
						_weather_theDay=mweather_data
					}
					weather_datas.push(mweather_data)
				}
				
			}
		}
		
		
		
		
		
		
	}
}