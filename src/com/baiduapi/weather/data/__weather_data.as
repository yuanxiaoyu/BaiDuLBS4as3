package com.baiduapi.weather.data
{
	/**
	 * 天气预报数据 
	 * @author 
	 * 
	 */	
	internal class __weather_data
	{	
		
		
		
		
		private var _date:String=""
		private var _dayPictureUrl:String=""
		private var _nightPictureUrl:String=""
		private var _weather:String=""
		private var _wind:String=""
		private var _temperature:String=""
		private var _minTemperature:Number=0
		private var _maxTemperature:Number=0
		
		
		/**
		 * 日期 
		 */	
		
		final public function get date():String{return _date}
		/**
		 * 白天的天气预报图片url 
		 */	
		
		final public function get dayPictureUrl():String{return _dayPictureUrl}
		/**
		 *  晚上的天气预报图片url 
		 */	
		
		final public function get nightPictureUrl():String{return _nightPictureUrl}
		/**
		 * 天气状况 
		 */	
		
		final public function get weather():String{return _weather}
		/**
		 * 风力 
		 */	
	
		final public function get wind():String{return _wind}
		/**
		 * 室外气温 
		 */	
	
		final public function get temperature():String{return _temperature}
		
		
		/**
		 * 室外最低气温
		 */	
		final public function get  minTemperature():Number{return _minTemperature}
		/**
		 * 室外最高气温
		 */	
		final public function get  maxTemperature():Number{return _maxTemperature}
		
		
		
		
		
		public function __weather_data(obj:Object=null)
		{
			if(obj!=null){
				_date=obj.date
				_dayPictureUrl=obj.dayPictureUrl
				_nightPictureUrl=obj.nightPictureUrl
				_weather=obj.weather
				_wind=obj.wind
				_temperature=obj.temperature
	
				var mtemperature:String=_temperature.substr(0,_temperature.indexOf("℃"))
				
				var arr:Array=mtemperature.split("~")
				if(arr.length==1){
					_minTemperature=arr[0]
					_maxTemperature=arr[0]
				}else{
					_minTemperature=Math.min(arr[0],arr[1])
					_maxTemperature=Math.max(arr[0],arr[1])
				}	
			}
			
		}
	}
}