package com.baiduapi.weather.data
{
	/**
	 * 天气指数 
	 * @author 偷心枫贼
	 * 
	 */	
	internal class __index
	{
		private var _des:String=""
		private var _title:String=""
		private var _zs:String=""
		private var _tipt:String=""
		/**
		 *  指数类型 
		 */	
		
		final public function get title():String{return _title}
		/**
		 *  指数取值 
		 */	
		
		final public function get zs():String{return _zs}
		/**
		 *指数含义 
		 */	
		
		final public function get tipt():String{return _tipt}
		/**
		 *指数详情 
		 */	

		final public function get des():String{return _des}
		
		
		public function __index(obj:Object=null)
		{
			if(obj!=null){
				_title=obj.title
				_zs=obj.zs
				_tipt=obj.tipt
				_des=obj.des
			}
		}
		
		
		
	}
}