package com.baiduapi.weather.data
{
	import com.baiduapi.weather.types.Pm25Type;
	
	
	/**
	 *@author 偷心枫贼
	 *        下午4:03:05
	 *
	 */
	internal class __pm25
	{
		
		private var _level:String=""
		private var _degree:String=""
		private var _color:uint=0
		private var _value:Number=0
		
		/**
		 * PM_级别 ,共六级,越高级污染越严重 ,请参考Pm25Type静态常量
		 */		
		
	    final public function get level():String{return _level}
		/**
		 * PM详情,描述 ,请参考Pm25Type静态常量
		 */		
		final public function get degree():String{return _degree}
		/**
		 * PM颜色 ,更直观的表达方式 ,请您直接用颜色绘制吧.    请参考Pm25Type静态常量
		 */		
		final public function get color():uint{return _color}
		
		/**
		 * PM25值,越高污染越严重  
		 * @return 
		 * 
		 */		
		final public function get value():Number{return _value}
		
		public function __pm25(pm:Number=NaN)
		{
		   if(pm){
			   _value=pm
				if(_value<=50){
					_level=Pm25Type.LV_1
					_degree=Pm25Type.OPTIMAL
					_color=Pm25Type.COLOR_GREEN
				}else if(_value>=51&&_value<=100){
					_level=Pm25Type.LV_2
					_degree=Pm25Type.GOOD
					_color= Pm25Type.COLOR_YELLOW
				}else if(_value>=101&&_value<=150){
					_level=Pm25Type.LV_3
					_degree=Pm25Type.LP
					_color=Pm25Type.COLOR_ORANGE
				}else if(_value>=151&&_value<=200){
					_level=Pm25Type.LV_4
					_degree=Pm25Type.MP
					_color=Pm25Type.COLOR_RED
				}else if(_value>=201&&_value<=300){
					_level=Pm25Type.LV_5
					_degree=Pm25Type.HP
					_color=Pm25Type.COLOR_PURPLE
				}else if(_value>=300){
					_level=Pm25Type.LV_6
					_degree=Pm25Type.SP
					_color=Pm25Type.COLOR_MAROON
				}	   
		   }
		}
	}
}