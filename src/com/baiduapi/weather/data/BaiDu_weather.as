package com.baiduapi.weather.data
{

	public class BaiDu_weather
	{
		private var _date:String=""

		/**
		 * 日期 
		 */		
		
		public function get date():String{return _date}
		/**
		 * 城市天气预报信息集合 
		 */		
		public const citys:Vector.<_city>=new Vector.<_city>();
		
		public function BaiDu_weather(obj:Object=null)
		{
			if(obj!=null){
				_date=obj.date
				var arr:Array=obj.results
				
				for(var i:int=0;i<arr.length;i++){
					var city:_city=new _city(arr[i])
					citys.push(city)
				}
			}			
		}
		
		
		
		
		public function toString():String{
			return JSON.stringify(this);
		}
	}
}
