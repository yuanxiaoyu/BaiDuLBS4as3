package com.baiduapi.weather.types
{
	
	
	/**
	 *@author 偷心枫贼
	 *        下午6:26:11
	 *
	 */
	public class Pm25Type
	{
		/**
		 * 优 
		 */		
		public static const OPTIMAL:String="优"
		/**
		 *良 
		 */			
		public static const GOOD:String="良"
		/**
		 *轻度污染 
		 */			
		public static const LP:String="轻度污染"
		/**
		 * 中度污染 
		 */			
		public static const MP:String="中度污染"
		/**
		 * 重度污染 
		 */			
		public static const HP:String="重度污染"
		/**
		 * 严重污染 
		 */			
		public static const SP:String="严重污染"
		
		
		public static const LV_1:String="一级"
		public static const LV_2:String="二级"
		public static const LV_3:String="三级"
		public static const LV_4:String="四级"
		public static const LV_5:String="五级"
		public static const LV_6:String="六级"
		
		
		/**
		 *  绿色 
		 */			
		public static const COLOR_GREEN:uint=0x00FF66
		/**
		 *黄色 
		 */			
		public static const COLOR_YELLOW:uint=0xFFFF00
		/**
		 * 橙色 
		 */			
		public static const COLOR_ORANGE:uint=0xF76809
		/**
		 * 红色 
		 */			
		public static const COLOR_RED:uint=0xFF0000
		/**
		 *紫色 
		 */			
		public static const COLOR_PURPLE:uint=0xA344BB
		/**
		 * 褐红色 
		 */		
		public static const COLOR_MAROON:uint=0x6D2D7D

			
			
			
	}
}