package com.baiduapi.weather.types
{
	
	
	/**
	 * 
	 * 天气状况TYPE
	 *@author 偷心枫贼
	 *        下午4:51:36
	 *
	 */
	public class WeatherType
	{
		
		public static const QING:String="晴"
		public static const DUO_YUN:String="多云"
		public static const YIN:String="阴"
		public static const ZHEN_YU:String="阵雨"
		public static const LEI_ZHEN_YU:String="雷阵雨"
		public static const LEI_ZHEN_YU$BING_SHUANG:String="雷阵雨伴有冰雹"
		public static const YU_JIA_XUE:String="雨夹雪"
		public static const XIAO_YU:String="小雨"
		public static const ZHONG_YU:String="中雨"
		public static const DA_YU:String="大雨"
		public static const BAO_YU:String="暴雨"
		public static const DA_BAO_YU:String="大暴雨"
		public static const TE_DA_BAO_YU:String="特大暴雨"
		public static const ZHEN_XUE:String="阵雪"
		public static const XIAO_XUE:String="小雪"
		public static const ZHONG_XUE:String="中雪"
		public static const DA_XUE:String="大雪"
		public static const BAO_XUE:String="暴雪"
		public static const WU:String="雾"
		public static const DONG_YU:String="冻雨"
		public static const SHA_CHEN_BAO:String="沙尘暴"
		public static const XIAO_YU_2_ZHON_GYU:String="小雨转中雨"
		public static const ZHONG_YU_2_DA_YU:String="中雨转大雨"
		public static const DA_YU_2_BAO_YU:String="大雨转暴雨"
		public static const BAO_YU_2_DA_BAO_YU:String="暴雨转大暴雨"
		public static const DA_BAO_YU_2_TE_DA_BAO_YU:String="大暴雨转特大暴雨"
		public static const XIAO_XUE_2_ZHONG_XUE:String="小雪转中雪"
		public static const ZHONG_XUE_2_DA_XUE:String="中雪转大雪"
		public static const DA_XUE_2_BAO_XUE:String="大雪转暴雪"
		public static const FU_CHEN:String="浮尘"
		public static const YANG_SHA:String="扬沙"
		public static const QIANG_SHA_CHEN_BAO:String="强沙尘暴"
		public static const MAI:String="霾"
			
			//晴|多云|阴|阵雨|雷阵雨|雷阵雨伴有冰雹|雨夹雪|小雨|中雨|大雨|暴雨|大暴雨|特大暴雨|阵雪|小雪|中雪|大雪|暴雪|雾|冻雨|沙尘暴|
			//小雨转中雨|中雨转大雨|大雨转暴雨|暴雨转大暴雨|大暴雨转特大暴雨|小雪转中雪|中雪转大雪|大雪转暴雪|浮尘|扬沙|强沙尘暴|霾
			
		
		
		
	}
}