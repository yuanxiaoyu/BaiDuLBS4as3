package com.baiduapi.location$ip.datas
{
	
	
	/**
	 * 详细地址
	 *@author 偷心枫贼
	 *        下午4:57:43
	 *
	 */
	internal class ___Address_detail
	{
		private var _city:String=""
		private var _city_code:uint=0
		private var _district:String=""
		private var _province:String=""
		private var _street:String=""
		private var _street_number:String=""
		
		
		
		/**
		 *城市 名字
		 */		
	     final  public function get city():String{return _city}
		/**
		 * 城市代码 
		 */	
		
		final public function get city_code():uint{return _city_code}
		/**
		 * 区 
		 */	
		
	    final public function get district():String{return _district}
		/**
		 * 省 
		 */	
		
		final public function get province():String{return _province}
		/**
		 * 街 
		 */	
		final public function get  street():String{return _street}
		/**
		 * 街道号码	 
		 */	
		
		final public function get street_number():String{return _street_number}
		
		
		
		public function ___Address_detail(obj:Object)
		{
			if(obj!=null){
				_city=obj.city
				_city_code=obj.city_code;
				_district=obj.district
				_province=obj.province
				_street=obj.street
				_street_number=obj.street_number
			}
			
		}
		
		
		
	}
}