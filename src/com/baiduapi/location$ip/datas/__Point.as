package com.baiduapi.location$ip.datas
{
	
	
	/**
	 * 位置坐标
	 *@author 偷心枫贼
	 *        下午5:28:26
	 *
	 */
	internal class __Point
	{
		private var _x:Number=0
		private var _y:Number=0
		/**
		 * x 
		 * @return 
		 * 
		 */		
		final public function get x():Number{return _x}
		/**
		 * y 
		 * @return 
		 * 
		 */		
		final public function get y():Number{return _y}
		
		
		public function __Point(obj:Object=null)
		{
			if(obj!=null){
				_x=obj.x
				_y=obj.y
			}
		}
		
		
		public function toLocationString():String{
			return x.toString()+","+y.toString();
		}
	}
	
	
}