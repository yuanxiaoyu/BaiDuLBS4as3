package com.baiduapi.location$ip.datas
{
	
	
	/**
	 * 详细内容
	 *@author 偷心枫贼
	 *        下午5:05:14
	 *
	 */
	internal class __Content
	{
		private  var _address:String=""
		private var _address_detail:___Address_detail
		private var _point:__Point=null
		
		/**
		 * 位置坐标 
		 * @return 
		 * 
		 */		
		final public function get point():__Point{return _point}
		
		/**
		 * 简要地址 
		 */	
	
		public function get address():String{return _address}
		/**
		 * 详细地址
		 */	
		
		public function get address_detail():___Address_detail{return _address_detail}

		
		
		public function __Content(obj:Object=null)
		{
			if(obj!=null){
				_address=obj.address
				_address_detail=new ___Address_detail(obj.address_detail)
				_point=new __Point(obj.point);
			}			
		}
	}
}