package com.baiduapi.location$ip.datas
{
	/**
	 * 百度位置数据 
	 * @author Administrator
	 * 
	 */	
	public class BaiDuLBS_Location
	{
		private var _address:String="未知"
		private var _content:__Content=null
	  
		/**
		 * 地址	 
		 */		
		
		final public function get address():String{return _address}
		/**
		 * 详细内容 
		 */		
		
		final public function get content():__Content{return _content}
		
		
		public function BaiDuLBS_Location(obj:Object=null)
		{
			if(obj!=null){
				_address=obj.address;
				_content=new __Content(obj.content);
				
			}
			
		}
		
		public function toString():String{
			return JSON.stringify(this);
		}
	}
}


