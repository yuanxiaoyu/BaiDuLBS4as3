package com.baiduapi.events
{
	
	import com.baiduapi.weather.data.BaiDu_weather;
	
	import flash.events.Event;
	
	
	/**
	 * 天气查询事件
	 *@author 偷心枫贼
	 *        下午5:59:58
	 *
	 */
	public class BaiduLBS_WeatherEvent extends Event
	{
		public static const COMPLETE:String="baidulbs_weather_event"
			
		/**
		 * 天气数据 
		 */			
		public var data:BaiDu_weather=null
			
		public function BaiduLBS_WeatherEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false,$data:BaiDu_weather=null)
		{
			data=$data;
			
			super(type, bubbles, cancelable);
		}
	}
}