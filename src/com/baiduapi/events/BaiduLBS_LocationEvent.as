package com.baiduapi.events
{
	import com.baiduapi.location$ip.datas.BaiDuLBS_Location;
	
	import flash.events.Event;
	/**
	 *  百度定位 事件
	 * @author 偷心枫贼
	 * 
	 */	
	public class BaiduLBS_LocationEvent extends Event
	{
		public static const COMPLETE:String="baidulbs_location_complete";
		
		/**
		 * 位置数据 
		 */		
		public var data:BaiDuLBS_Location=null;
		
		public function BaiduLBS_LocationEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false,$data:BaiDuLBS_Location=null)
		{
			this.data=$data;		
			super(type, bubbles, cancelable);
		}
	}
}